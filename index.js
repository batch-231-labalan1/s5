

class Product{
	constructor(name, price){
		this.name = name;
		this.price = price;
		this.isActive = true;
	}

	archive(){
		this.isActive = false;
		return this;
	}

	updatePrice(newPrice){
		this.price = newPrice;
		return this;
	}
}



class Cart{
	constructor(){
		this.contents = [];
		this.totalAmount = 0;

	}

	addToCart(product, quantity){
		let item = {
			product: product,
			quantity: quantity
		}
		this.contents.push(item);
		return this;
	}

	showCartContents(){
		console.log(this.contents);
		return this;
	}

	updateProductQuantity(productName, newQuantity){

    this.contents.forEach(product =>{
            if (product.product.name == productName){
                product.quantity = newQuantity;
            }
        })
        
        this.computeTotal();
        return this;

        	// if(this.product === productName){
        	// 	this.quantity = newQuantity;
        	// }
        	// return this;
    }

    clearCartContents(){
    	this.contents = [];
    	this.totalAmount = 0;
    	return this;
    }

    computeTotal(){

     let itemPrice = 0
		this.contents.forEach(product =>{
			// console.log(item.product.price);
			// console.log(item.quantity);
			itemPrice += product.product.price * product.quantity;
		});

		this.totalAmount = itemPrice;
		return this;
    }
}




class Customer{

	constructor(email){
		this.email = email;
		this.cart = new Cart("Cart1");
		this.orders = [];
	}

	checkOut(){
	if(this.cart.length !== 0){
		this.cart.computeTotal();
			
		this.orders.push({
            products: this.cart.contents,
            totalAmount: this.cart.totalAmount
        })

	  }
		return this;
	}
}








// TEST STATEMENTS

let ace = new Customer("ace@gmail.com");

const prodA = new Product("pillow",250);
const prodB = new Product("lotion",300);
// console.log(prodA);
// console.log(prodB);

ace.cart.addToCart(prodA, 2)
ace.cart.addToCart(prodA, 3)

//compute total
ace.cart.updateProductQuantity('pillow', 5)
ace.cart.showCartContents()


ace.checkOut()


















